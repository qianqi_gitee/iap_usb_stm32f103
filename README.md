# iap_usb_stm32f103

#### 介绍
带usb，fatfs的iap升级例子

#### 软件架构
软件架构说明!
使用stm32cube自带的freertosv1框架，实现串口打印任务以及文件系统挂载并升级的任务同时进行。
usb使用stm32的usb外设，作为device从机，实现了一个u盘，大小200kb。u盘存储介质为stm32内部flash区域。
程序使用fatfs去读取当前盘符下的bin文件，并依次写入到内部flash，完毕后再将当前的bin文件删除掉，避免下一次重启又执行相同的操作。
同样，可以使用之前写好的iap上位机来查看串口打印消息。

![格式化](photo16da1278ba24e2bfce6ab84b0b28934d.png)![输入图片说明](1731490337091_B5B75033-DECC-4517-82BB-73D7893F00FD.png)
![输入图片说明](1731490794699_F6666BB5-B35B-424f-8342-8E083F12F052.png)

#### 格式化
 
u盘个格式化可以在电脑上进行，选择fat格式即可

#### 使用说明

 在首次没有文件系统时或者没有相应的文件时，会打印相应的信息

 


#### 说明

芯片型号stm32f103vet6，
内部flash分为三部分，boot+app+flash区域，具体可以在代码中的usb以及fatfs的读写接口中查看。
整体功能可用，但不能使用文件系统创建文件以及文件夹，暂时还不知道为啥，仅能读取。有人能查一下的话就再好不过了。在下看了两天没看出来。。
fatfs+usb+iap,无需使用外部flash芯片就可以完成整个升级流程，且拖拽文件到u盘的操作比较实用，相信一定有所帮助。

