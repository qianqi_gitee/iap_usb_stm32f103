#ifndef __CRC_H
#define __CRC_H

#include <stdbool.h>
#include <stdint.h>
#include <main.h>
#include "usart.h"
#define WORD uint16_t
#define BYTE uint8_t


uint16_t crc16_ibm_step(uint16_t crc, uint8_t* data, uint32_t length);
 
uint16_t crc16_ibm(uint8_t* data, uint32_t length);
#endif
