#include "crc_smx.h"

uint16_t crc16_ibm_step(uint16_t crc, uint8_t* data, uint32_t length)
{
    uint8_t i;

    // uint16_t crc = 0xffff;        // Initial value
    if(length > 2014)
        return 0;

    while (length--)
        {
            crc ^= *data++; // crc ^= *data; data++;

            for (i = 0; i < 8; ++i)
                {
                    if (crc & 1)
                        crc = (crc >> 1) ^ 0xA001; // 0xA001 = reverse 0x8005
                    else
                        crc = (crc >> 1);
                }
        }

    return crc;
}

uint16_t crc16_ibm(uint8_t* data, uint32_t length)
{

    return crc16_ibm_step(0xffff, data, length);
}