#include "iap.h"
#include "ff.h"			/* Declarations of FatFs API */
#include "diskio.h"		/* Declarations of disk I/O functions */
#include "usart.h"
#include "string.h"
#include "stdio.h"
#include "cmsis_os.h"
#include "flash.h"

iap_st iap;

FRESULT f_res; /* 文件操作结果 */

FATFS fs;
uint8_t val = 0;
uint8_t file_buff[1024] = {0};
uint8_t uart_send_buff[1024] = {0};
uint8_t *buff = &val;
FIL file;
UINT fnum;
uint32_t file_len;
uint32_t file_pack;

void usb_iap(void)
{

    f_res = f_mount(&fs, "0:", 1);

    if(f_res == FR_OK)
        {

            f_res = f_open(&file, "0:download.bin", FA_OPEN_EXISTING | FA_READ | FA_WRITE);
            osDelay(1000);

            if(f_res == FR_OK)
                {
                    file_len = f_size(&file);
                    memset((char *)uart_send_buff, 0, 1024);
                    sprintf((char *)uart_send_buff, "文件长度=%d\r\n", file_len);
                    usart_send_str((char *)uart_send_buff, &print_usart );
                    osDelay(1000);

                    if(file_len % 1024 == 0)
                        file_pack = file_len / 1024;
                    else
                        file_pack = file_len / 1024 + 1;

                    memset((char *)uart_send_buff, 0, 1024);
                    sprintf((char *)uart_send_buff, "文件包数=%d\r\n", file_pack);
                    usart_send_str((char *)uart_send_buff, &print_usart );
                    osDelay(5000);

                    for(uint8_t i = 0; i < file_pack; i++)
                        {
                            f_lseek (&file, 1024 * i );
                            f_res = f_read(&file, file_buff, 1024, &fnum);
                            // usart_send_hex((char *)file_buff, &print_usart, fnum );

                            /*此处添加flash相关代码*/
                            memset((char *)uart_send_buff, 0, 1024);
                            sprintf((char *)uart_send_buff, "正在写入flash：%d/%d\r\n", i, file_pack);
                            usart_send_str((char *)uart_send_buff, &print_usart );
                            flash_write_with_erase( 0x8010000 + 1024 * i, file_buff, fnum );

                            osDelay(100);
                        }

                    osDelay(1000);
                    usart_send_str((char *)" 删除当前文件\r\n", &print_usart );
                    f_unlink ("0:download.bin");//删除当前文件
                    usart_send_str((char *)" 即将进入app\r\n", &print_usart );
                    osDelay(1000);
                    jump_to_app( );

                }
            else
                usart_send_str((char *)" 读取失败\r\n", &print_usart );
        }
    else
        {
            usart_send_str((char *)"挂载失败\r\n", &print_usart );

        }
}


