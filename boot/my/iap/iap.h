#ifndef __IAP_H
#define __IAP_H

#include <stdbool.h>
#include <stdint.h>
#include <main.h>

typedef struct
{
    uint8_t cur_pack;//当前包数
    uint8_t total_pack;//升级总包数
    uint8_t iap_flag;//boot标志 默认=0 在等待阶段置1后  表示此次需要升级 不自动进入app
	  uint32_t iap_reset_delay;//标记当前时间  在一段时间后自动重启
	
} iap_st;
extern iap_st iap;


void usb_iap(void);
#endif
