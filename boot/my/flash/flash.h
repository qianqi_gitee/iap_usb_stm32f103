#ifndef __FLASH_H
#define __FLASH_H

#include <stdbool.h>
#include <stdint.h>
#include <main.h>

//需要与擦除参数FLASH_SECTOR_5对应
#define STM_FLASH_ADDR 0x08020000
//扇区 0     0x0800 0000 - 0x0800 3FFF 16 KB
//扇区 1     0x0800 4000 - 0x0800 7FFF 16 KB
//扇区 2     0x0800 8000 - 0x0800 BFFF 16 KB
//扇区 3     0x0800 C000 - 0x0800 FFFF 16 KB
//扇区 4     0x0801 0000 - 0x0801 FFFF 64 KB
//扇区 5     0x0802 0000 - 0x0803 FFFF 128 KB
//扇区 6     0x0804 0000 - 0x0805 FFFF 128 KB

typedef struct
{
    uint8_t id[7];//桩编号
    uint8_t ip1;
    uint8_t ip2;
    uint8_t ip3;
    uint8_t ip4;
    uint16_t port;

} config_st;

extern config_st config;
void flash_write(uint32_t add, uint8_t *data, uint16_t len);
void flash_write_with_erase(uint32_t add, uint8_t *data, uint16_t len);
void flash_write_with_read(uint32_t add, uint8_t *data, uint16_t len);
uint8_t STMFLASH_ReadWord(uint32_t faddr);
void flash_read(uint32_t ReadAddr, uint8_t *data, uint16_t len);
void read_prarm(void);
void write_prarm(void);
#endif
