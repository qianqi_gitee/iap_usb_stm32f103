#include "flash.h"
#include "stdio.h"
#include "string.h"

config_st config;

uint64_t test2 = 0;

void flash_write(uint32_t add, uint8_t *data, uint16_t len)
{
    uint64_t *test;
    // uint32_t TypeErase;   /*!< TypeErase: Mass erase or page erase.
    // This parameter can be a value of @ref FLASHEx_Type_Erase */
    //
    // uint32_t Banks;       /*!< Select banks to erase when Mass erase is enabled.
    // This parameter must be a value of @ref FLASHEx_Banks */
    //
    // uint32_t PageAddress; /*!< PageAdress: Initial FLASH page address to erase when mass erase is disabled
    // This parameter must be a number between Min_Data = 0x08000000 and Max_Data = FLASH_BANKx_END
    // (x = 1 or 2 depending on devices)*/
    //
    // uint32_t NbPages;
    FLASH_EraseInitTypeDef FlashSet;

    //test=(uint64_t *)data;
    //	test2 =*((volatile unsigned int *)test);//?0x0028FF1C就是内存中的地址，根据自己需要指定
    //
//    if(add % 0x800 == 0)
//        {
            uint32_t PageError = 0;
            FlashSet.Banks = FLASH_BANK_1;//全片擦才有用

            FlashSet.PageAddress = add; //地址
            FlashSet.TypeErase = FLASH_TYPEERASE_PAGES; //按页
            FlashSet.NbPages = 1; //1page
            HAL_FLASH_Unlock();
            HAL_FLASHEx_Erase(&FlashSet, &PageError);
//        }
//    else
//        HAL_FLASH_Unlock();

    for(uint16_t i = 0; i < len/8; i ++)
        {

          test = (uint64_t *)data + i;
            HAL_FLASH_Program( FLASH_TYPEPROGRAM_DOUBLEWORD, add + i*8, *((volatile uint64_t *)test));
					  

        }

    HAL_FLASH_Lock();

}

void flash_write_with_erase(uint32_t add, uint8_t *data, uint16_t len)
{
    uint64_t *test;
    // uint32_t TypeErase;   /*!< TypeErase: Mass erase or page erase.
    // This parameter can be a value of @ref FLASHEx_Type_Erase */
    //
    // uint32_t Banks;       /*!< Select banks to erase when Mass erase is enabled.
    // This parameter must be a value of @ref FLASHEx_Banks */
    //
    // uint32_t PageAddress; /*!< PageAdress: Initial FLASH page address to erase when mass erase is disabled
    // This parameter must be a number between Min_Data = 0x08000000 and Max_Data = FLASH_BANKx_END
    // (x = 1 or 2 depending on devices)*/
    //
    // uint32_t NbPages;
    FLASH_EraseInitTypeDef FlashSet;

    //test=(uint64_t *)data;
    //	test2 =*((volatile unsigned int *)test);//?0x0028FF1C就是内存中的地址，根据自己需要指定
    //
    if(add % 0x800 == 0)
        {
            uint32_t PageError = 0;
            FlashSet.Banks = FLASH_BANK_1;//全片擦才有用

            FlashSet.PageAddress = add; //地址
            FlashSet.TypeErase = FLASH_TYPEERASE_PAGES; //按页
            FlashSet.NbPages = 1; //1page
            HAL_FLASH_Unlock();
            HAL_FLASHEx_Erase(&FlashSet, &PageError);
        }
    else
        HAL_FLASH_Unlock();

    for(uint16_t i = 0; i < len/8; i ++)
        {

          test = (uint64_t *)data + i;
            HAL_FLASH_Program( FLASH_TYPEPROGRAM_DOUBLEWORD, add + i*8, *((volatile uint64_t *)test));
					  

        }

    HAL_FLASH_Lock();

}



void flash_write_with_read(uint32_t add, uint8_t *data, uint16_t len)
{
		uint32_t buff[2048/4];	
    uint64_t *test;
    uint32_t *s;
	 uint8_t *p;
	
	 uint32_t temp;
    FLASH_EraseInitTypeDef FlashSet;

    //test=(uint64_t *)data;
    //	test2 =*((volatile unsigned int *)test);//?0x0028FF1C就是内存中的地址，根据自己需要指定
    //
	uint16_t secpos;
	uint16_t secoff;
	uint16_t secremain;	   
 	uint16_t i;    
//	u8 * W25QXX_BUF;	  
//   	W25QXX_BUF=W25QXX_BUFFER;	     
 	secpos=(add-(0x8000000 + 0x10000+1024*200))/2048;//扇区地址  
	secoff=(add-(0x8000000 + 0x10000+1024*200))%2048;//在扇区内的偏移
	
	
	
	//先读出所有数据
	   s = (uint32_t *)(0x8000000 + 0x10000+1024*200+secpos*2048);
   for(int i = 0; i < 1*2048/4 ; i++) //读取FLash地址中的数据
        {
            buff[i] = *(s++ );
        }
				
	
	  p = (uint8_t *)&buff[0];
				memcpy(p+secoff,data,len);
				
				
	
	 
            uint32_t PageError = 0;
            FlashSet.Banks = FLASH_BANK_1;//全片擦才有用

            FlashSet.PageAddress = add; //地址
            FlashSet.TypeErase = FLASH_TYPEERASE_PAGES; //按页
            FlashSet.NbPages = 1; //1page
            HAL_FLASH_Unlock();
            HAL_FLASHEx_Erase(&FlashSet, &PageError);
     
  p = (uint8_t *)(0x8000000 + 0x10000+1024*200+secpos*2048);
 	
    for(uint16_t i = 0; i < 2048/4; i ++)
        {

//      temp=((uint32_t)*(buff+i)<<24)|((uint32_t)*(buff+i)>>8<<24>>8)|((uint32_t)*(buff+i)>>16<<24>>16)|((uint32_t)*(buff+i)>>24);
		  temp=*(buff+i);
				
            HAL_FLASH_Program( FLASH_TYPEPROGRAM_WORD, (uint32_t)p ,  temp);
					  	p += 4;

        }
				
				
//				  s =(0x8000000 + 0x10000+1024*200+secpos*2048);
//				temp=((uint32_t)*(data)<<24)|((uint32_t)*(data+1)<<16)|((uint32_t)*(data+2)<<8)|((uint32_t)*(data+3)<<0);
//    for(uint16_t i = 0; i < len/4; i ++)
//        {

//        
//            HAL_FLASH_Program( FLASH_TYPEPROGRAM_WORD, add+ i*4, temp);
//					  

//        }
				
				

    HAL_FLASH_Lock();

}
uint8_t STMFLASH_ReadWord(uint32_t faddr)//读取指定地址的字节
{
    return *(uint8_t*)faddr;//faddr：读地址  返回值：对应数据
}

void flash_read(uint32_t add, uint8_t *data, uint16_t len)
{
    uint16_t i;

    for(i = 0; i < len; i++)
        {
            *data = STMFLASH_ReadWord(add);
            data += 1;
            add += 1;
        }
}

void read_prarm(void)
{

    flash_read(STM_FLASH_ADDR, (uint8_t *)&config, sizeof(config));

}

void write_prarm(void)
{

    flash_write(STM_FLASH_ADDR, (uint8_t *)&config, sizeof(config));
}
