#include "usart.h"
#include "stdio.h"
#include "string.h"
//#include "cmsis_os.h"
#include "crc_smx.h"
#include "iap.h"
#include "flash.h"

uart_tx_st print_usart;// 串口1  打印串口

/*
串口绑定硬件
在回调函数中根据地址对应
*/
void usart_init(void)
{
    print_usart.uart_p = &huart1;
    print_usart.send_out_item = 0;
    print_usart.send_in_item = 0;

}

void usart_send_task(uart_tx_st *uart)
{
    if(uart->send_in_item != uart->send_out_item && uart->uart_state == ready)
        {
            uart->uart_state = runing;
            HAL_UART_Transmit_DMA(uart->uart_p, (uint8_t *)&uart->send_buff[uart->send_out_item][0], uart->len[uart->send_out_item]);
        }

}

void usart_send_str(char *p, uart_tx_st *uart)
{

    uint16_t len = strlen((char *)p);

    if(len > MAX_LEN) len = MAX_LEN;
//    taskENTER_CRITICAL();
    memcpy((char *)&uart->send_buff[uart->send_in_item][0], p, len);
    uart->len[uart->send_in_item] = len;
    uart->send_in_item = get_circlr_in(MAX_ITEM, uart);
//    taskEXIT_CRITICAL();

}

void usart_send_hex(char *p, uart_tx_st *uart, uint16_t len)
{

    if(len > MAX_LEN) len = MAX_LEN;

//    taskENTER_CRITICAL();
    memcpy((char *)&uart->send_buff[uart->send_in_item][0], p, len);
    uart->len[uart->send_in_item] = len;
    uart->send_in_item = get_circlr_in(MAX_ITEM, uart);
//    taskEXIT_CRITICAL();

}

uint8_t get_circlr_in(uint8_t max, uart_tx_st *uart)
{
    uart->send_in_item = uart->send_in_item < (max - 1) ? uart->send_in_item + 1 : 0;
    return uart->send_in_item	;

}
uint8_t get_circlr_out(uint8_t max, uart_tx_st *uart)
{
    uart->send_out_item = uart->send_out_item < (max - 1) ? uart->send_out_item + 1 : 0;
    return uart->send_out_item	;

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    uart_tx_st *uart_this;
    if(huart == print_usart.uart_p)
        {
            uart_this = &print_usart;
        }

    uart_this->send_out_item = get_circlr_out(MAX_ITEM, uart_this);
    uart_this->uart_state = ready;

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    uart_tx_st *uart_this;
    if(huart == print_usart.uart_p)
        {
            uart_this = &print_usart;
        }

    uart_this->rx_timecnt = HAL_GetTick();
    uart_this->rxdata[uart_this->rx_cnt] = uart_this->rx_byte;
    HAL_UART_Receive_IT(uart_this->uart_p, &uart_this->rx_byte, 1);

    if(uart_this->rx_cnt < 1024)
        uart_this->rx_cnt++;

}

void jump_to_app(void)
{
    typedef void (*app_func_t)(void);
    uint32_t app_addr = OTA_CODE_START_ADD;
    uint32_t stk_addr = *((__IO uint32_t *)app_addr);
    app_func_t app_func = (app_func_t)(*((__IO uint32_t *)(app_addr + 4)));

    if ((((uint32_t)app_func & 0xff000000) != 0x08000000) || ((stk_addr & 0x2ff00000) != 0x20000000))
        {
            return;
        }

    //rt_kprintf("Jump to application running ... \n");
    // rt_thread_mdelay(200);

    __disable_irq(); /*关闭总中断*/
    HAL_DeInit(); /*函数通过写复位寄存器，将所有模块复位。*/

    for(int i = 0; i < 128; i++)
        {
            HAL_NVIC_DisableIRQ(i); /*失能中断*/
            HAL_NVIC_ClearPendingIRQ(i);/*清除中断标志位*/
        }

    /* 关闭滴答定时器，复位到默认值 */
    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL = 0;

    HAL_RCC_DeInit(); /*将RCC时钟配置重置为默认重置状态。*/

    __set_CONTROL(0); /*设置CONTROL寄存器的值。在RTOS工程，这条语句很重要，设置为特权级模式，使用MSP指针*/
    __set_MSP(stk_addr); /* 设置MSP跳转的地址；设置主堆栈指针 */
    app_func(); /* Jump to application running */
}
void print_usart_handle(void)
{
    uint8_t boot[6] = {'b', 'o', 'o', 't'};
    uint8_t bin_ask[6] = {0};
    uint8_t crc_ask[6] = {0};
    uint32_t test = 0;
    uint32_t *p;
    uint16_t crc = crc16_ibm(print_usart.rxdata, print_usart.rx_cnt - 2);
    //	p=(uint32_t *)boot[3];
    //	test=*p;
    if(print_usart.rx_cnt == 1024)
        {
            memcpy(print_usart.rxdata_backup, print_usart.rxdata, 1024);
            print_usart.crc_backup = crc16_ibm(print_usart.rxdata_backup, 1024);
            bin_ask[0] = 0;
            bin_ask[1] = 0;
            bin_ask[2] = iap.cur_pack;
            bin_ask[3] = iap.total_pack;
            crc = crc16_ibm(bin_ask, 4);
            bin_ask[4] = crc;
            bin_ask[5] = crc >> 8;
            usart_send_hex((char *)&bin_ask, &print_usart, 6);
            return;
        }

    if(print_usart.rxdata[print_usart.rx_cnt - 2] != (crc & 0x00ff) || print_usart.rxdata[print_usart.rx_cnt - 1] != (crc >> 8))
        return;

    //boot
    if(print_usart.rxdata[0] == 'b' && print_usart.rxdata[1] == 'o' && print_usart.rxdata[2] == 'o' && print_usart.rxdata[3] == 't')
        {

            crc = crc16_ibm(boot, 4);
            boot[4] = crc;
            boot[5] = crc >> 8;

            usart_send_hex((char *)&boot, &print_usart, 6);
            iap.iap_flag = 1;//让boot不自动进入app
        }
    //test
    if(print_usart.rxdata[0] == 't' && print_usart.rxdata[1] == 'e' && print_usart.rxdata[2] == 's' && print_usart.rxdata[3] == 't')
        {
            HAL_NVIC_SystemReset();
        }

    if(print_usart.rxdata[0] == iap.cur_pack && print_usart.rx_cnt == 6)
        {
            if(print_usart.rxdata[2] != (print_usart.crc_backup & 0x00ff) || print_usart.rxdata[3] != (print_usart.crc_backup >> 8))
                {
                    crc_ask[0] = 1;
                    crc_ask[1] = 0;
                    crc_ask[2] = iap.cur_pack;
                    crc_ask[3] = iap.total_pack;
                    crc = crc16_ibm(crc_ask, 4);
                    crc_ask[4] = crc;
                    crc_ask[5] = crc >> 8;
                    usart_send_hex((char *)&crc_ask, &print_usart, 6);
                    return;
                }

            /*此处添加flash相关代码*/
            flash_write_with_erase( 0x8010000 + 1024 * iap.cur_pack, print_usart.rxdata_backup, 1024 );

            iap.total_pack = print_usart.rxdata[1] ;
            iap.cur_pack++;

            crc_ask[0] = 1;
            crc_ask[1] = 1;
            crc_ask[2] = iap.cur_pack;
            crc_ask[3] = iap.total_pack;
            crc = crc16_ibm(crc_ask, 4);
            crc_ask[4] = crc;
            crc_ask[5] = crc >> 8;
            usart_send_hex((char *)&crc_ask, &print_usart, 6);

            /*所有包接收完成 自动重启*/
            if( iap.cur_pack >= iap.total_pack)
              iap.iap_reset_delay=HAL_GetTick();

        }

}
