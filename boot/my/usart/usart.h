#ifndef __USART_H
#define __USART_H

#include <stdbool.h>
#include <stdint.h>
#include <main.h>
#define  MAX_LEN 1024
#define  MAX_ITEM 8
#define OTA_CODE_START_ADD     (0x8010000)   /* app 区域储存的首地址 */
/*串口状态枚举*/
enum state
{
    ready,
    runing,

} ;
typedef struct
{
    uint8_t* txdata;//发送地址指针
    uint8_t rx_byte;//接受暂存
    uint8_t rxdata[1024];//接受缓存数组
    uint8_t rxdata_backup[1024];//备份
    uint16_t crc_backup;//接受字节数
    uint32_t rx_timecnt;//接受的时间戳
    uint16_t rx_cnt;//接受字节数

    UART_HandleTypeDef *uart_p;//绑定的硬件串口指针
    uint8_t send_in_item;//填充序列
    uint8_t send_out_item;//发送序列
    enum state uart_state;//串口状态
    uint16_t len[MAX_ITEM];//发送长度记录
    uint8_t send_buff[MAX_ITEM][MAX_LEN];//128字节发送缓存  32组
} uart_tx_st;

void usart_init(void);
void usart_send_task(uart_tx_st *uart);
uint8_t get_circlr_in(uint8_t max, uart_tx_st *uart);
uint8_t get_circlr_out(uint8_t max, uart_tx_st *uart);
void usart_send_str(char *p, uart_tx_st *uart);
void usart_send_hex(char *p, uart_tx_st *uart, uint16_t len);
void print_usart_handle(void);
void jump_to_app(void);
extern uart_tx_st print_usart;

extern UART_HandleTypeDef huart1;
 

#endif
